# -*- coding: utf-8 -*-
"""
This file is part of the AutoBackups package.

(c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
"""
import hashlib
import os
import re
import shutil
import threading
import time

import sublime
import sublime_plugin

from .paths_helper import PathsHelper

from python_utils import logging_system
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.events import Events
from python_utils.sublime_text_utils.queue import Queue

__all__ = [
    "AutoBackupsForkEventListener",
    "AutoBackupsForkOpenBackupCommand",
    "AutoBackupsForkOpenBackupsFolderCommand",
    "AutoBackupsForkToggleLoggingLevelCommand",
]

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
        )
    )
)

queue = Queue()
events = Events()

package_name = os.path.basename(root_folder)
plugin_name = "AutoBackupsFork"
logger: logging_system.Logger = logging_system.Logger(
    logger_name=plugin_name,
    use_file_handler=os.path.join(root_folder, "tmp", "logs"),
)
settings = settings_utils.SettingsManager(
    settings_file=plugin_name,
    events=events,
    logger=logger,
)


class Storage:
    """Storage class.

    Attributes
    ----------
    view_hashes : str
        Description
    """

    view_hashes = {}


def set_logging_level():
    """Summary"""
    try:
        logger.set_logging_level(logging_level=settings.get("logging_level", "ERROR"))
    except Exception as err:
        print(__file__, err)


@events.on("plugin_loaded")
def on_plugin_loaded():
    """Summary"""
    queue.debounce(
        settings.load,
        delay=100,
        key=f"{plugin_name}-debounce-settings-load",
    )
    queue.debounce(
        set_logging_level,
        delay=200,
        key=f"{plugin_name}-debounce-set-logging-level",
    )
    queue.debounce(
        initialize,
        delay=500,
        key=f"{plugin_name}-debounce-initialize",
    )
    queue.debounce(
        remove_old_backups,
        delay=10000,
        key=f"{plugin_name}-debounce-garbage-collection",
    )


@events.on("plugin_unloaded")
def on_plugin_unloaded():
    """Summary"""
    settings.unobserve()
    queue.unload()
    events.destroy()


@events.on("settings_changed")
def on_settings_changed(settings_obj, **kwargs):
    """Summary

    Parameters
    ----------
    settings_obj : TYPE
        Description
    **kwargs
        Description
    """
    if settings_obj.has_changed("logging_level"):
        queue.debounce(
            set_logging_level,
            delay=1000,
            key=f"{plugin_name}-debounce-settings-changed",
        )


def initialize():
    backup_dir = settings.get("backup_dir", {}).get(sublime.platform(), "")
    backup_per_day = settings.get("backup_per_day")
    backup_per_time = settings.get("backup_per_time")
    backup_name_mode = settings.get("backup_name_mode")

    PathsHelper.initialize(
        backup_dir, backup_per_day, backup_per_time, backup_name_mode
    )
    logger.info(
        "Initialized with the following settings:\n"
        + f"- backup_dir: {backup_dir}\n"
        + f"- backup_per_day: {backup_per_day}\n"
        + f"- backup_per_time: {backup_per_time}\n"
        + f"- backup_name_mode: {backup_name_mode}"
    )


def remove_old_backups():
    backup_time = settings.get("delete_old_backups", 0)
    if backup_time > 0:
        thread = AutoBackupsForkRemoveOldBackups(backup_time)
        thread.start()


class AutoBackupsForkEventListener(sublime_plugin.EventListener):
    def on_pre_save_async(self, view):
        self._save_backup(view, 0)

    def on_load_async(self, view):
        if settings.get("backup_on_open_file"):
            self._save_backup(view, 1)

    def _save_backup(self, view, on_load_event):
        if view.is_read_only():
            return

        view_size = view.size()
        max_backup_file_size = settings.get("max_backup_file_size_bytes")
        if view_size is None:
            logger.debug("Backup aborted. View size not available.")
            return

        if max_backup_file_size is None:
            logger.debug("Backup aborted. Max size allowed by config not available.")
            return

        # don't save files above configured size
        if view_size > max_backup_file_size:
            logger.warning("Backup aborted. File too large (%d bytes)." % view.size())
            return

        filename = view.file_name()
        if filename is None:
            logger.debug("Backup aborted. Trying to save a view without a file.")
            return

        # Check file path in excluded regexes
        if self._is_excluded(filename):
            logger.debug("Backup aborted. File is excluded.")
            return

        # not create file backup if current file is backup
        if on_load_event & self._is_backup_file(filename):
            logger.debug("Backup aborted. Trying to save an already backed up file.")
            return

        newname = PathsHelper.get_backup_filepath(filename)
        if newname is None:
            logger.debug(
                "Backup aborted. New name for backed up file couldn't be generated."
            )
            return

        buffer_id = view.buffer_id()
        content = filename + view.substr(sublime.Region(0, view_size))
        content = self._encode(content)
        current_hash = hashlib.md5(content).hexdigest()

        last_hash = ""
        try:
            last_hash = Storage.view_hashes[buffer_id]
        except Exception:
            last_hash = ""

        # not create file backup if no changes from last backup
        if last_hash == current_hash:
            logger.debug("Backup aborted. Hashes are identical.")
            return

        # not create file if exists
        if on_load_event & os.path.isfile(newname):
            logger.debug("Backup aborted. Backup file exists.")
            return

        (backup_dir, file_to_write) = os.path.split(newname)

        if os.access(backup_dir, os.F_OK) is False:
            os.makedirs(backup_dir)

        try:
            shutil.copy(filename, newname)
        except FileNotFoundError:
            logger.error("Backup aborted. File " + filename + " does not exist!")
            return False

        Storage.view_hashes[buffer_id] = current_hash
        logger.debug("Backup saved to: " + newname.replace("\\", "/"))

    def _is_backup_file(self, path):
        backup_per_time = settings.get("backup_per_time")
        path = PathsHelper.normalise_path(path)
        base_dir = PathsHelper.get_base_dir(False)
        base_dir = PathsHelper.normalise_path(base_dir)
        if backup_per_time == "folder":
            base_dir = base_dir[:-7]

        backup_dir_len = len(base_dir)
        sub = path[0:backup_dir_len]

        if sub == base_dir:
            return True
        else:
            return False

    def _is_excluded(self, filename):
        # check
        ignore_regexes = settings.get("ignore_regexes")

        if ignore_regexes is None or ignore_regexes == "":
            return False

        for regex in ignore_regexes:
            prog = re.compile(".*" + regex + ".*")
            result = prog.match(filename)
            if result is not None:
                return True

        return False

    def _encode(self, text):
        if isinstance(text, str):
            text = text.encode("UTF-8")

        return text


class AutoBackupsForkOpenBackupCommand(sublime_plugin.TextCommand):
    datalist = []
    curline = 1

    def run(self, edit):
        backup_per_day = settings.get("backup_per_day")

        window = self.view.window()
        view = self.view

        open_in_same_line = settings.get("open_in_same_line", True)
        if open_in_same_line:
            (row, col) = view.rowcol(view.sel()[0].begin())
            self.curline = row + 1

        if not backup_per_day:
            filepath = view.file_name()
            newname = PathsHelper.get_backup_filepath(filepath)
            if os.path.isfile(newname):
                window.open_file(newname)
            else:
                sublime.error_message("Backup for " + filepath + " does not exist!")
        else:
            f_files = self.get_data(False)

            if not f_files:
                sublime.error_message("Backups for this file do not exist!")
                return

            backup_per_time = settings.get("backup_per_time")
            if backup_per_time:
                window.show_quick_panel(f_files, self.time_folders)
            else:
                window.show_quick_panel(f_files, self.open_file)
            return

    def get_data(self, time_folder):
        filename = PathsHelper.normalise_path(self.view.file_name(), True)
        basedir = PathsHelper.get_base_dir(True)

        backup_per_time = settings.get("backup_per_time")
        if backup_per_time:
            if backup_per_time == "folder":
                f_files = []
                if time_folder is not False:
                    tm_folders = self.get_data(False)
                    tm_folder = tm_folders[time_folder][0]
                    basedir = basedir + "/" + tm_folder

                    if not os.path.isdir(basedir):
                        sublime.error_message("Folder " + basedir + " not found!")

                    for folder in os.listdir(basedir):
                        fl = basedir + "/" + folder + "/" + filename
                        match = re.search(r"^[0-9+]{6}$", folder)
                        if os.path.isfile(fl) and match is not None:
                            folder_name, file_name = os.path.split(fl)
                            f_file = []
                            time = self.format_time(folder)
                            f_file.append(time + " - " + file_name)
                            f_file.append(fl)
                            f_files.append(f_file)
                else:
                    path, flname = os.path.split(filename)
                    (filepart, extpart) = os.path.splitext(flname)
                    for folder in os.listdir(basedir):
                        match = re.search(r"^[0-9+]{4}-[0-9+]{2}-[0-9+]{2}$", folder)
                        if match is not None:
                            folder_name, file_name = os.path.split(filename)
                            f_file = []
                            basedir2 = basedir + "/" + folder
                            count = 0
                            last = ""
                            for folder2 in os.listdir(basedir2):
                                match = re.search(r"^[0-9+]{6}$", folder2)
                                if match is not None:
                                    basedir3 = (
                                        basedir
                                        + "/"
                                        + folder
                                        + "/"
                                        + folder2
                                        + "/"
                                        + filename
                                    )
                                    if os.path.isfile(basedir3):
                                        count += 1
                                        last = folder2
                            if count > 0:
                                f_file.append(folder)
                                f_file.append(
                                    "Backups: "
                                    + str(count)
                                    + ", Last edit: "
                                    + self.format_time(last)
                                )
                                f_files.append(f_file)
            elif backup_per_time == "file":
                f_files = []
                if time_folder is not False:
                    tm_folders = self.get_data(False)
                    tm_folder = tm_folders[time_folder][0]
                    path, flname = os.path.split(filename)
                    basedir = basedir + "/" + tm_folder + "/" + path
                    (filepart, extpart) = os.path.splitext(flname)

                    if not os.path.isdir(basedir):
                        sublime.error_message("Folder " + basedir + " not found!")

                    for folder in os.listdir(basedir):
                        fl = basedir + "/" + folder
                        match = re.search(
                            r"^"
                            + re.escape(filepart)
                            + "_([0-9+]{6})"
                            + re.escape(extpart)
                            + "$",
                            folder,
                        )

                        if os.path.isfile(fl) and match is not None:
                            time = self.format_time(match.group(1))
                            f_file = []
                            f_file.append(time + " - " + flname)
                            f_file.append(fl)
                            f_files.append(f_file)
                else:
                    path, flname = os.path.split(filename)
                    (filepart, extpart) = os.path.splitext(flname)
                    for folder in os.listdir(basedir):
                        match = re.search(r"^[0-9+]{4}-[0-9+]{2}-[0-9+]{2}$", folder)
                        if match is not None:
                            folder_name, file_name = os.path.split(filename)
                            f_file = []
                            basedir2 = basedir + "/" + folder + "/" + path
                            count = 0
                            last = ""
                            if os.path.isdir(basedir2):
                                for sfile in os.listdir(basedir2):
                                    match = re.search(
                                        r"^"
                                        + re.escape(filepart)
                                        + "_([0-9+]{6})"
                                        + re.escape(extpart)
                                        + "$",
                                        sfile,
                                    )
                                    if match is not None:
                                        count += 1
                                        last = match.group(1)
                            if count > 0:
                                f_file.append(folder)
                                f_file.append(
                                    "Backups: "
                                    + str(count)
                                    + ", Last edit: "
                                    + self.format_time(last)
                                )
                                f_files.append(f_file)
        else:
            f_files = []
            for folder in os.listdir(basedir):
                fl = basedir + "/" + folder + "/" + filename
                match = re.search(r"^[0-9+]{4}-[0-9+]{2}-[0-9+]{2}$", folder)
                if os.path.isfile(fl) and match is not None:
                    folder_name, file_name = os.path.split(fl)
                    f_file = []
                    f_file.append(folder + " - " + file_name)
                    f_file.append(fl)
                    f_files.append(f_file)
        f_files.sort(key=lambda x: x[0])
        f_files.reverse()
        self.datalist = f_files
        return f_files

    def time_folders(self, parent):
        if parent == -1:
            return

        # open file
        f_files = self.get_data(parent)

        if settings.get("show_previews", True):
            sublime.set_timeout_async(
                lambda: self.view.window().show_quick_panel(
                    f_files, self.open_file, on_highlight=self.show_file
                ),
                100,
            )
        else:
            sublime.set_timeout_async(
                lambda: self.view.window().show_quick_panel(f_files, self.open_file),
                100,
            )

        return

    def show_file(self, file):
        if file == -1:
            return

        f_files = self.datalist
        filename = f_files[file][1]
        window = self.view.window()

        view = window.open_file(
            filename + ":" + str(self.curline),
            sublime.ENCODED_POSITION | sublime.TRANSIENT,
        )
        view.set_read_only(True)

    def open_file(self, file):
        if file == -1:
            window = sublime.active_window()
            window.focus_view(self.view)
            return

        f_files = self.datalist
        filename = f_files[file][1]

        window = self.view.window()
        view = window.open_file(
            filename + ":" + str(self.curline), sublime.ENCODED_POSITION
        )
        view.set_read_only(True)
        window.focus_view(view)

    def format_time(self, time):
        time = time[0:2] + ":" + time[2:4] + ":" + time[4:6]
        return time


class AutoBackupsForkRemoveOldBackups(threading.Thread):
    backup_time = 0

    def __init__(self, back_time):
        self.backup_time = back_time
        threading.Thread.__init__(self)

    def run(self):
        import datetime

        basedir = PathsHelper.get_base_dir(True)
        backup_time = self.backup_time

        if backup_time < 1:
            return

        diff = (backup_time + 1) * 24 * 3600
        deleted = 0
        now_time = time.time()
        for folder in os.listdir(basedir):
            match = re.search(r"^[0-9]{4}-[0-9]{2}-[0-9]{2}$", folder)
            if match is not None:
                folder_time = time.mktime(
                    datetime.datetime.strptime(folder, "%Y-%m-%d").timetuple()
                )
                if now_time - folder_time > diff:
                    fldr = basedir + "/" + folder
                    try:
                        shutil.rmtree(fldr, onerror=self.onerror)
                        deleted = deleted + 1
                    except Exception as err:
                        logger.error(err)

        if deleted > 0:
            diff = backup_time * 24 * 3600
            dt = now_time - diff
            date = datetime.datetime.fromtimestamp(dt).strftime("%Y-%m-%d")
            logger.debug(
                "AutoBackupsFork: Deleted "
                + str(deleted)
                + " backup folders older than "
                + date
            )

    def onerror(self, func, path, exc_info):
        import stat

        if not os.access(path, os.W_OK):
            # Is the error an access error ?
            os.chmod(path, stat.S_IWUSR)
            func(path)
        else:
            raise


class AutoBackupsForkOpenBackupsFolderCommand(sublime_plugin.WindowCommand):
    def run(self, paths=[]):
        backup_dir = os.path.expandvars(os.path.expanduser(PathsHelper.backup_dir))

        if sublime.platform() == "windows":
            import subprocess

            if self.isDirectory():
                subprocess.Popen(["explorer", self.escape_windows(backup_dir)])
            else:
                subprocess.Popen(
                    ["explorer", "/select,", self.escape_windows(backup_dir)]
                )
        else:
            sublime.active_window().run_command("open_dir", {"dir": backup_dir})

    def escape_windows(self, string):
        return string.replace("^", "^^")


class AutoBackupsForkToggleLoggingLevelCommand(
    settings_utils.SettingsToggleList, sublime_plugin.ApplicationCommand
):
    """Summary"""

    def __init__(self, *args, **kwargs):
        """Summary

        Parameters
        ----------
        *args
            Arguments.
        **kwargs
            Keyword arguments.
        """
        sublime_plugin.ApplicationCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleList.__init__(
            self,
            key="logging_level",
            settings=settings,
            description="Logging level - {}",
            values_list=["ERROR", "INFO", "DEBUG"],
        )


if __name__ == "__main__":
    pass
