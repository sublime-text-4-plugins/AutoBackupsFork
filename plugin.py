# -*- coding: utf-8 -*-
"""
This file is part of the AutoBackups package.

(c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
"""
from .plugins import *  # noqa
from .plugins import events
from .plugins import settings
from python_utils.sublime_text_utils import settings as settings_utils


class AutoBackupsForkProjectSettingsController(settings_utils.ProjectSettingsController):
    """Project settings controller.
    """
    @settings_utils.distinct_until_buffer_changed
    def on_post_save_async(self, view):
        """Called after a view has been saved.

        Parameters
        ----------
        view : sublime.View
            A Sublime Text ``View`` object.
        """
        self._on_post_save_async(view, settings)


def plugin_loaded():
    """On plugin loaded callback."""
    events.broadcast("plugin_loaded")


def plugin_unloaded():
    """On plugin unloaded."""
    events.broadcast("plugin_unloaded")


if __name__ == "__main__":
    pass
